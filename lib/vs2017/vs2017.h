#pragma once

#ifdef VS2017_EXPORTS
#define VS2017_API __declspec(dllexport)
#else
#define VS2017_API __declspec(dllimport)
#endif

// 此类是从 vs2017.dll 导出的
class VS2017_API Cvs2017 {
public:
	Cvs2017(void);
	// TODO:  在此添加您的方法。
};

extern VS2017_API int nvs2017;
VS2017_API int fnvs2017(void);
