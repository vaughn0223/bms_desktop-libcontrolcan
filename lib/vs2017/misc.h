#pragma once

#include <string>


std::string get_exe_fullpath();
std::string get_exe_filename();
std::string get_exe_directory();