#include <Windows.h>
#include <boost/filesystem.hpp>

#include "misc.h"


// from ATL 7.0 sources
#ifndef _delayimp_h
extern "C" IMAGE_DOS_HEADER __ImageBase;	// windows.h
#endif

std::string get_exe_fullpath() {
	char path[MAX_PATH];
	const auto h_module = reinterpret_cast<HMODULE>(&__ImageBase);
	GetModuleFileNameA(h_module, path, MAX_PATH);
	return std::string(path);
}

std::string get_exe_filename() {
	boost::filesystem::path path(get_exe_fullpath());
	auto tmp = path.filename().string();
	return path.filename().string();
}

std::string get_exe_directory() {
	boost::filesystem::path path(get_exe_fullpath());
	auto tmp = path.parent_path().string();
	return path.parent_path().string();
}