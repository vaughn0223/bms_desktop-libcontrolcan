// vs2017.cpp : 定义 DLL 应用程序的导出函数。
//

#include "vs2017.h"


// 这是导出变量的一个示例
VS2017_API int nvs2017=0;

// 这是导出函数的一个示例。
VS2017_API int fnvs2017(void)
{
	return 42;
}

// 这是已导出类的构造函数。
// 有关类定义的信息，请参阅 vs2017.h
Cvs2017::Cvs2017()
{
	return;
}
